'use strict';

/*EXERCICE SLIDE 33*/
/*EXEMPLES SURCHARGE DE CONSTRUCTEUR*/
//constructeur 1
/*function Projet(name, category){
    this.name = name;
    this.category = category;
}

//constructeur 2
function Projet(name, category, date){
    this.name = name;
    this.category = category;
    this.date = date;
}

//constructeur 3
function Projet(name, category, date, description){
    this.name = name;
    this.category = category;
    this.date = date;
    this.description = description;
}*/

//constructeur 4
function Projet(name, category, date, description, urlProj){
    this.name = name;
    this.category = category;
    this.date = date;
    this.description = description;
    this.urlProj = urlProj;
}
