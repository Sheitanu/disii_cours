'use strict';


$("#mailForm").validate({
    rules: {
        nameContact: {
            required: true,
            minlength: 3
        },
        firstnameContact: {
            required: true,
            minlength: 3
        },
        mailContact: {
            email:true,
            required: true
        },
    },
    messages: {
        nameContact: {
            required: "Veuillez saisir votre nom s'il vous plaît.",
            minlength: "Votre nom doit être composé au minimum de 3 caractères."            
        },
        firstnameContact: {
            required: "Veuillez saisir votre prénom s'il vous plaît.",
            minlength: "Votre prénom doit être composé au minimum de 3 caractères."
        },
        mailContact: {
            required: "Veuillez saisir une adresse email s'il vous plaît.",
            email: "Veuillez entrer une adresse email valide s'il vous plaît."
        }        
    }
});
