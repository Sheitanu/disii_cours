//obligatoire depuis ES6
'use strict';

//debugger;
/*Exercices de base*/
/*let message = 'pour le test';
 let age = 20;
 //affichage d'une boîte de dialogue - tout le temps visible
 //est-ce que age > 17 ? oui, on affiche 'Vous êtes majeur.', non, on affiche 'Vous êtes mineur';
 alert(age > 17 ? 'Vous êtes majeur.' : 'Vous êtes mineur.');
 //affichage d'une information en console - seulement visible en mode dev
 console.log('je suis un log ' + message);*/


//Exercice slide 26
/*let tab = [2, 6, 9, 8];
 tab.forEach(function(element){
 console.log("element = " +element);
 });
 for(let j in tab){
 console.log("indice " + j);
 console.log("valeur " + tab[j]);
 } 
 //init variables
 let res = 0;
 let resChaine = "";
 //for
 for (let i = 0; i < tab.length; i++) {
 //on fait le calcul
 res += tab[i]; //res = res + tab[i];
 //Si on est au dernier élément, on affiche =
 if (i === tab.length - 1) {
 resChaine += tab[i] + " = "; //resChaine = resChaine + tab[i] + "="
 } else {
 //sinon on affiche +
 resChaine += tab[i] + " + "; //resChaine = resChaine + tab[i] + "+"
 }
 }
 //2 + 6 + 9 + 8 = 25 où resChaine = '2 + 6 + 9 + 8 = ' et res = 25
 console.log(resChaine + res);*/

/*Exercice slide 27*/
/*PROJECTS*/
//Récupérer tous les éléments dont l'attribut name = 'projects-type'
/*let projectsType = document.getElementsByName('projects-type');
 for (let i = 0; i < projectsType.length; i++) {
 //Récupère chaque élément HTML un par un
 let projectType = projectsType[i];
 //Vérifie s'il est sélectionné
 if (projectType.checked) {
 /*TODO : test sur projectType.value*/
/* if (projectType.value == "vitrine") {
 alert('vitrine');
 } else if (projectType.value == "commerce") {
 alert('commerce');
 } else if (projectType.value == "all") {
 alert('all');
 }*/
/* switch (projectType.value) {
 case 'vitrine':
 alert('vitrine');
 break;
 case 'commerce' :
 alert('commerce');
 break;
 default :
 alert('all');
 break;
 }
 }
 }*/
/*function displayProjets(){
    let projectsType = document.getElementsByName("projects-type");
    //on récupère l'ensemble des projets en fonction de leur classe
    let all = document.getElementsByClassName("all");
    let vitrine = document.getElementsByClassName("vitrine");
    let commerce = document.getElementsByClassName("commerce");
    for (let i = 0; i < projectsType.length; i++) {
        //Récupère chaque élément HTML
        let projectType = projectsType[i];
        //Vérifie si il est sélectionné
        if (projectType.checked) {
            if (projectType.value === "vitrine") {
                //hide all projects
                for (let a = 0; a < all.length; a++) {
                    all[a].style.display = 'none';
                }
                //display only vitrine projects
                for (let c = 0; c < vitrine.length; c++) {
                    vitrine[c].style.display = 'block';
                }
            } else if (projectType.value === "commerce") {
                for (let a = 0; a < all.length; a++) {
                    all[a].style.display = 'none';
                }
                for (let c = 0; c < commerce.length; c++) {
                    commerce[c].style.display = 'block';
                }
            } else if (projectType.value === "all") {
                for (let a = 0; a < all.length; a++) {
                    all[a].style.display = 'block';
                }
            }
        }
    }
}*/

/*EXERCICE SLIDE 33*/
let proj = new Projet("essai", "category");
console.log(proj.name);
console.log(proj.category);
console.log(proj);

let proj1 = new Projet("essai1", "", "20-03-2020");
console.log(proj1.name);
console.log(proj1.category);
console.log(proj1.date);
console.log(proj1);

let proj3 = new Projet("essai3", "cat", "20-03-2020", "desc3", "url");
console.log(proj3.name);
console.log(proj3.category);
console.log(proj3.date);
console.log(proj3.description);
console.log(proj3.urlProj);
console.log(proj3);
//Modification d'un attribut de l'objet
proj3.urlProj = "url2";
console.log(proj3.urlProj);


/*EXERCICE SLIDE 36*/
let jsonProj3 = JSON.stringify(proj3);
console.log(jsonProj3);

/*EXERCICE SLIDE 37*/
//let proj4String = "{\"name\":\"essai4\", \"category\":\"cat1\", \"date\":\"26-03-2020\", \"description\":\"desc1\", \"urlProj\":\"url3\", \"autre\":\"descAutre\"}";
let proj4String = '[{"name":"essai4", "category":"cat1", "date":"26-03-2020", \n\
"description":"desc1", "urlProj":"url3", "autre":"descAutre"}, \n\
{"name":"essai4", "category":"cat1", "date":"26-03-2020", \n\
"description":"desc1", "urlProj":"url3", "autre":"descAutre"}]';
console.log(proj4String);
let proj4JSON = JSON.parse(proj4String);
console.log(proj4JSON.name);
console.log(proj4JSON.autre);
console.log(proj4JSON.champInvalide);
console.log(proj4JSON);
