'use strict';
/**Quelques mots dès la page d’accueil**/
/*let count = 0;
 let wordsArray = ["Persévérante", "Joyeuse", "Sérieuse", "Volontaire"];
 let colors = ["#FF0000", "#FFFF00", "red", "blue", "grey"];
 setInterval(function () {
 count++;
 document.getElementById("words").innerHTML = wordsArray[count % wordsArray.length];
 let w = document.getElementById("words");
 w.style.color = colors[count % colors.length];
 }, 2500);*/
let count = 0;
let wordsArray = ["Persévérante", "Joyeuse", "Sérieuse", "Volontaire"];
let colors = ["#FF0000", "#FFFF00", "red", "blue", "grey"];
setInterval(function () {
    count++;
    $("#words").fadeOut(600, function () {
        $(this).html(wordsArray[count % wordsArray.length])
                .css('color', colors[count % colors.length])
                .css('font-size', '30px')
                .fadeIn(600);
    });
}, 2500);


/**Vous plus en détail en version jQuery**/
let titre = $('#titre-about');
titre.html('<b>Marie GUYOTON</b>');
$('#description-about').html('Développeur web, front (JS, HTML, CSS, AngularJS) & back (JAVA, PHP)');
$('#img-about').attr('src', './img/bg-signup.jpg');

function addDiv() {
    //la fonction créée une div et l'insère après la div dont l'id est description-about
    $('#description-about')
            .after($('<div>',
                    {id: 'test-div', html: 'Youhou !', class: 'text-white-50'}));
}

function delDiv() {
    //la fonction supprime la div dont l'id est test-div
    $('#test-div').remove();
}

/**Projets**/
function displayProjets() {
    /*Version js*/
    /* let projectsType = document.getElementsByName("projects-type");
     let all = document.getElementsByClassName("all");
     let vitrine = document.getElementsByClassName("vitrine");
     let commerce = document.getElementsByClassName("commerce");*/
    /*Version jQuery*/
    let projectsType = $("[name='projects-type'");
    let all = $(".all");
    let vitrine = $(".vitrine");
    let commerce = $(".commerce");
    for (let i = 0; i < projectsType.length; i++) {
        //Récupère chaque élément HTML
        let projectType = projectsType[i];
        //Vérifie si il est sélectionné
        if (projectType.checked) {
            if (projectType.value === "vitrine") {
                for (let a = 0; a < all.length; a++) {
                    all[a].style.display = 'none';
                }
                for (let c = 0; c < vitrine.length; c++) {
                    vitrine[c].style.display = 'block';
                }
            } else if (projectType.value === "commerce") {
                for (let a = 0; a < all.length; a++) {
                    all[a].style.display = 'none';
                }
                for (let c = 0; c < commerce.length; c++) {
                    commerce[c].style.display = 'block';
                }
            } else if (projectType.value === "all") {
                for (let a = 0; a < all.length; a++) {
                    all[a].style.display = 'block';
                }
            }
        }
    }
}


/**Mail**/
function testMail(e) {
    let mailInput = document.getElementById("mail-util");
    //suppr, - , _, @, ., .(pavé numérique), a-z, 0-9, - (pavé numérique)
    if (e.keyCode === 8 || e.keyCode === 48 || e.keyCode === 54 || e.keyCode === 56 || e.keyCode === 59 || e.keyCode === 110
            || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 109) {
        console.log('touche ok');
        //mettre la bordure de l'input normale        
        mailInput.style.border = 'solid 0.5px rgba(0,0,0,.5)';
    } else {
        // alert("Cette touche n'est pas autorisée dans les adresses mails.");
        //récupère la div et on met une bordure rouge
        mailInput.style.border = 'solid';
        mailInput.style.borderColor = 'red';
    }
}
/*version js
 function testEnvoi(){*/
//Appel au click sur le bouton dont l'id est #send-msg
$('#send-msg').click(function () {
    if ($('#mailForm').valid()) {
        $.ajax({
            type: "POST",
            url: 'php/envoi_mail.php',
            dataType: 'json',
            data: {
                from: $("#mail-util").val(),
                body: $("#body-msg").val(),
                name: $("#name-msg").val(),
                firstname: $("#fname-msg").val()
            },
            success: function (obj, textstatus) {
                $('#msg-toast').html(obj.success);
                $('.toast').toast('show');
                console.log(obj);
            },
            fail: function (obj, textstatus) {
                $('#msg-toast').html(obj.success);
                $('.toast').toast('show');
                console.log(obj);
            }
        });
    } else {
        $('#msg-toast').html('Formulaire invalide !');
        $('.toast').toast('show');
        //alert('Formulaire invalide !');
    }
});

$('.toast').toast('hide');