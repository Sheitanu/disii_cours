<?php

header('Content-Type: application/json');

error_reporting(E_ALL ^ E_STRICT);
ini_set('display_errors', 'On');

$res['error'] = 'Oups, une erreur est survenue.';
if (isset($_POST['from']) && isset($_POST['body']) && isset($_POST['name']) && isset($_POST['firstname'])) {
    $to = 'mdagniau@almeri.fr';
    $subject = 'Quelques informations';
    $message = $_POST['body'];
    $headers = 'From: ' . $_POST['firstname'] . ' ' . $_POST['name'] . ' <' . $_POST['from'] . ">\r\n" .
            'Reply-To: ' . $_POST['firstname'] . ' ' . $_POST['name'] . ' <' . $_POST['from'] . ">\r\n" .
            'X-Mailer: PHP/' . phpversion();

    $success = mail($to, $subject, $message, $headers);
    if ($success) {
        $res['success'] = 'Le mail a été envoyé avec succès.';
        $res['error'] = '';
    } else {
        $res['error'] = 'Une erreur s\'est produite lors de l\'envoi du mail. ' + $mail->ErrorInfo;
    }
}

echo(json_encode($res));
