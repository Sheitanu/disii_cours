<?php
header('Content-Type: application/json');

//msg d'erreur par défaut
$res['error'] = 'Un paramètre est manquant.';

//test si param est là
if(isset($_POST['test'])){
    //blabla
    $res['error'] = '';
    $res['success'] = 'L\'appel au fichier ajax.php est ok.';
}

echo json_encode($res);
